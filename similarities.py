"""
Testing for search similar documents
"""

import sys
import csv
import numpy as np

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


CORPUS_FILE = './data/snopes.csv'

LIMIT = 10


def get_corpus():
    corpus = []
    with open(CORPUS_FILE) as f_csv:
        reader = csv.DictReader(f_csv)
        for row in reader:
            corpus.append(row['claim'])
    return corpus


def find_similar_documents(query, limit):
    corpus = [query]
    corpus += get_corpus()
    vectorizer = TfidfVectorizer(min_df=0)
    model = vectorizer.fit_transform(corpus)
    cosine_sim = cosine_similarity(model, model)
    sim_scores = list(enumerate(cosine_sim[0]))  # qry index: 0
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    results = []
    for sim in sim_scores[1:limit+1]:
        results.append(corpus[sim[0]])
    return results


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please use as follow: python similarities.py "<query>" <limit:opt>')
        sys.exit(0)

    query = sys.argv[1]
    limit = LIMIT

    if len(sys.argv) > 2:
        try:
            limit = int(sys.argv[2])
            if not limit > 0:
                raise ValueError
        except ValueError:
            print('Limit should be an integer > 0')
            sys.exit(1)

    results = find_similar_documents(query, limit)

    print('\n'.join(results))
